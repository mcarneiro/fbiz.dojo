Dojo F.biz

Reunião de todos os Dojos feitos na F.biz. Caso queira treinar algum problema ou melhorar alguma solução, siga os passos:

1. Faça um fork deste repositório;
2. Clone na sua máquina;
3. Edite e atualize seu repositório;
4. Envie e discuta sua solução na [nossa comunidade](http://discourse.fbiz.com.br/) para receber feedbacks.