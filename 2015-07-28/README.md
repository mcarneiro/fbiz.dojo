# Dojo - 28/07/2015

Primeiro dojo da F.biz, feito em JS.

## Problema - Jokenpo

Jokenpo é uma brincadeira japonesa, onde dois jogadores escolhem um dentre três possíveis itens: Pedra, Papel ou Tesoura.
O objetivo é fazer um juiz de Jokenpo que dada a jogada dos dois jogadores informa o resultado da partida.

As regras são as seguintes:

* Pedra empata com Pedra e ganha de Tesoura
* Tesoura empata com Tesoura e ganha de Papel
* Papel empata com Papel e ganha de Pedra

_Retirado de [http://dojopuzzles.com/problemas/exibe/jokenpo/](http://dojopuzzles.com/problemas/exibe/jokenpo/)_

## Participantes

* Adriano Enache
* Carlos Rossignatti
* Eduardo Aragão
* Eduardo Magaldi
* Igor Almeida
* Kauê Righi
* Leandro Ribeiro
* Marcelo Carneiro
* Marcelo Vieira
* Marcos Garcia
* Nadia Araujo
* Nicholas Almeida
* Renato Inamine

## :)

* Participação bem ativa da equipe;
* Dinâmina;
* Sugestões de teste — como os problemas que parecem simples tem diversos desdobramentos;
* O exercício avançou bem mesmo com participantes que não conheciam de programação;
* Colocando o TDD (programação orientada à testes) em prática;

## :(

* 1h30 foi pouco tempo — não conseguimos refatorar;
* Perdemos 10 minutos no começo por causa dos atrasos;
* Algumas pessoas ficaram confusas com o editor estar apenas na tela grande;
* Usuários de Windows ficaram confusos com o teclado;
* Algumas pessoas não falavam o que estavam fazendo durante seu momento de piloto;

## Melhorar

* Deixar o enunciado do problema visível sempre;
* Deixar o computador com monitor espelhado;
* Colocar também um teclado ABNT2;
* Começar pontualmente para que seja possível todos participarem mais de uma vez;
* Pedir pão de queijo;
* Fazer com 2 horas de duração, das 9:00 às 11:00;
