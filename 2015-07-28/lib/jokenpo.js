function jokenpo(jogador1, jogador2) {

	if(arguments.length != 2) {
		return "erro";
	}

	if(typeof jogador1 != "string")
		return "erro";
    if(typeof jogador2 != "string")
		return "erro";


	jogador1 = jogador1.toLowerCase().trim();
	jogador2 = jogador2.toLowerCase().trim();

	if (jogador1 != "pedra" && jogador1 != "tesoura" && jogador1 != "papel"){
		return "erro";
	}

	if (jogador2 != "pedra" && jogador2 != "tesoura" && jogador2 != "papel"){
		return "erro";
	}

	if (jogador1 == "pedra" && jogador2 == "tesoura"){
		return "pedra";
	}
	if (jogador1 == "tesoura" && jogador2 == "pedra"){
		return "pedra";
	}
	if (jogador1 == "tesoura" && jogador2 == "papel"){
		return "tesoura";
	}
	if (jogador1 == "papel" && jogador2 == "tesoura"){
		return "tesoura";
	}
	if (jogador1 == "papel" && jogador2 == "pedra"){
		return "papel";
	}
	if (jogador1 == "pedra" && jogador2 == "papel"){
		return "papel";
	}
	if (jogador1 == jogador2){
		return "empate";
	}

}

module.exports = jokenpo;